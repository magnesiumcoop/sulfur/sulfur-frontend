import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HoraDetalle } from '../../_models/HoraDetalle';

@Component({
  selector: 'app-dialog-hora-detalle',
  templateUrl: './dialog-hora-detalle.component.html',
  styleUrls: ['./dialog-hora-detalle.component.scss']
})
export class DialogHoraDetalleComponent {

  constructor(public dialogRef: MatDialogRef<DialogHoraDetalleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: [string, HoraDetalle[]]) { }

    Close() {
      this.dialogRef.close();
    }
}
